import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/services/user_service.dart';

import 'globals.dart' as globals;

class MobileApiClient extends ApiClient {
  MobileApiClient()
      : super(
          Uri.parse(globals.chatApiAddress),
          onBeforeRequest: (ApiRequest request) {
            if (UserService.instance.authToken != null) {
              return request.change(
                headers: {}
                  ..addAll(request.headers)
                  ..addAll(
                    {'authorization': UserService.instance.authToken},
                  ),
              );
            }

            return request;
          },
          onAfterResponse: (ApiResponse response) {
            if (response.headers.containsKey('authorization')) {
              UserService.instance.setToken(response.headers['authorization']);
            }

            return response;
          },
        );
}
