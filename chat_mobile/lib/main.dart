import 'package:chat_mobile/services/chat_service.dart';
import 'package:chat_mobile/ui/splash/splash_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'globals.dart' as globals;

Future<void> main() async {
  await Hive.initFlutter();

  Get.put(ChatComponent(globals.webSocketAddress));
  runApp(SimpleChatApp());
}

class SimpleChatApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Simple Chat',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: const SplashView(),
    );
  }
}
