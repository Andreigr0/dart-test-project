import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:hive/hive.dart';

class UserRepository {
  static const userBoxKey = 'user';
  static const _tokenKey = 'tokenKey';
  static const _userKey = 'userKey';

  Box get userBox => Hive.box(userBoxKey);

  Future<String> getToken() async {
    return userBox.get(_tokenKey) as String;
  }

  Future<void> saveToken(String userToken) async {
    await userBox.put(_tokenKey, userToken);
    logger.i('Saved token');
  }

  Future<User> getUser() async {
    // Workaround. It should be HiveObject
    final user = userBox.get(_userKey);
    if (user == null) {
      return null;
    }

    final userJson = user.cast<String, dynamic>() as Map<String, dynamic>;
    return User.fromJson(userJson);
  }

  Future<void> saveUser(User user) async {
    await userBox.put(_userKey, user?.json);
    logger.i('Saved user ${user?.json}');
  }

  Future<void> clear() async => userBox.clear();
}
