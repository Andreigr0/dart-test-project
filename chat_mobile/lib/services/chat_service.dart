library chat_component;

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:get/get.dart';

typedef NotificationCallback<T> = void Function(T valueObject);

class _ChatMessageStreamControllerWrapper {
  final ChatId chatId;
  final StreamController<Message> streamController;

  _ChatMessageStreamControllerWrapper(this.chatId, this.streamController);
}

/// The component incapsulates websocket connection and provides api to subscribe
/// for new messages in some particular chat and for notifications about unread messages in any chat.
/// If there is a subscription for messages in a chat this class assume there is no unread
/// messages in this chat.
/// Clients should invoke [StreamSubscription.cancel] when stopped listening the stream.
///
/// Should be disposed after usage.
class ChatComponent extends GetxController {
  static ChatComponent get instance => Get.find<ChatComponent>();

  final String _address;

  ChatComponent(this._address);

  WebSocket _webSocket;
  StreamSubscription _wsSubscription;

  final _unreadChats = HashSet<ChatId>();

  List<_ChatMessageStreamControllerWrapper> _messageStreamControllers = [];

  List<StreamController<Set<ChatId>>> _unreadMesNotifControllers = [];

  int _retryAttempts = 0;

  @override
  void onInit() {
    super.onInit();
    connect();
  }

  @override
  Future<void> onClose() async {
    for (final controller in _unreadMesNotifControllers) {
      if (!controller.isClosed) {
        controller.close();
      }
    }
    _unreadMesNotifControllers = null;

    for (final controller in _messageStreamControllers) {
      if (!controller.streamController.isClosed) {
        controller.streamController.close();
      }
    }

    _messageStreamControllers = null;

    await _wsSubscription?.cancel();
    _webSocket?.close();

    super.onClose();
  }

  Future<void> connect() async {
    try {
      final WebSocket webSocket = await WebSocket.connect(_address);

      _retryAttempts = 0;

      _wsSubscription = webSocket.listen((data) {
        logger.d(data);

        if (data is String) {
          final decode = jsonDecode(data) as Map<String, dynamic>;
          final receivedMessage = Message.fromJson(decode);

          final ChatId chatId = receivedMessage.chat;
          bool messageConsumed = false;

          for (final controller in _messageStreamControllers) {
            // TODO remove closed controllers
            if (!controller.streamController.isClosed &&
                (controller.chatId == chatId)) {
              messageConsumed = true;
              controller.streamController.sink.add(receivedMessage);
            }
          }

          if (!messageConsumed) {
            _unreadChats.add(chatId);
            _notifyUnread();
          }
        }
      });
    } catch (err) {
      // simple websocket reconnect policy
      logger.e(err);
      if (_retryAttempts <= 3) {
        _retryAttempts++;
        await Future.delayed(5.seconds);
        connect();
      }
    }
  }

  StreamSubscription<Message> subscribeMessages(
    NotificationCallback<Message> callback,
    ChatId chatId,
  ) {
    _unreadChats.remove(chatId);
    // assume all messages are read in this chat
    _notifyUnread();

    final streamController = StreamController<Message>();

    _messageStreamControllers.add(
      _ChatMessageStreamControllerWrapper(chatId, streamController),
    );

    streamController.onCancel = () => streamController.close();

    return streamController.stream.listen(callback);
  }

  StreamSubscription<Set<ChatId>> unreadSubscription(
      NotificationCallback<Set<ChatId>> callback) {
    final streamController = StreamController<Set<ChatId>>();

    _unreadMesNotifControllers.add(streamController);
    streamController.onCancel = () => streamController.close();

    final result = streamController.stream.listen(callback);

    streamController.sink.add(_unreadChats);
    return result;
  }

  void _notifyUnread() {
    for (final controller in _unreadMesNotifControllers) {
      if (!controller.isClosed) {
        // TODO remove closed controllers
        controller.sink.add(_unreadChats);
      }
    }
  }
}
