import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/repositories/user_repository.dart';
import 'package:chat_mobile/ui/chat/chat_view.dart';
import 'package:chat_mobile/ui/login/login_view.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:get/get.dart';

import '../api_client.dart';

User get currentUser => UserService.instance.currentUser;

class UserService extends GetxController {
  static UserService get instance => Get.find<UserService>();

  UserRepository get _userRepo => Get.find<UserRepository>();

  String authToken;
  User currentUser;

  @override
  Future<void> onInit() async {
    super.onInit();
    authToken = await _userRepo.getToken();
    currentUser = await _userRepo.getUser();

    if (authToken == null || currentUser == null) {
      Get.offAll(const LoginView());
    } else {
      logger.d('Loaded user ${currentUser.json}');
      Get.offAll(const ChatView());
    }
  }

  Future<void> setUser(User user) async {
    currentUser = user;
    await _userRepo.saveUser(currentUser);
    logger.d('UserService Saved user ${currentUser?.json}');
  }

  Future<void> setToken(String token) async {
    authToken = token;
    await _userRepo.saveToken(token);
  }

  Future<void> logout() async {
    _userRepo.clear();
    await setToken(null);
    await setUser(null);
    await Get.offAll(const LoginView());
    logger.d('Logout');
  }

  Future<void> refreshToken() async {
    final UsersClient usersClient = UsersClient(MobileApiClient());
    await usersClient.login(currentUser.name, currentUser.password);
  }
}
