import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoadingState<T extends GetxBaseViewModel> extends StatelessWidget {
  final String tag;
  final VoidCallback retryAction;
  final bool disabled;
  final EdgeInsets padding;
  final Widget child;
  final bool showText;

  const LoadingState({
    this.tag,
    this.retryAction,
    this.disabled = false,
    this.padding,
    this.child,
    this.showText = true,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final edgeInsets = padding ?? const EdgeInsets.all(8.0);

    return GetBuilder<T>(
      builder: (T model) {
        if (model.isBusy) {
          return Container(
            width: double.infinity,
            padding: edgeInsets,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CircularProgressIndicator(strokeWidth: 1),
                if (showText) ...[
                  const SizedBox(height: 12),
                  const Text('Loading'),
                ],
              ],
            ),
          );
        } else if (model.hasError) {
          if (retryAction == null && !model.showDialogOnError) {
            return Padding(
              padding: edgeInsets,
              child: Center(
                child: Text(
                  model.errorMessage,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            );
          }

          if (retryAction != null) {
            return Container(
              width: double.infinity,
              padding: padding ??
                  const EdgeInsets.symmetric(
                    vertical: 8,
                    horizontal: 16,
                  ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  if (!model.showDialogOnError) ...[
                    Text(
                      model.errorMessage,
                      textAlign: TextAlign.center,
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(height: 16),
                  ],
                  RaisedButton(
                    onPressed: disabled ? null : retryAction,
                    child: const Text('Retry'),
                  ),
                ],
              ),
            );
          }
        }
        return child ?? Container();
      },
    );
  }
}
