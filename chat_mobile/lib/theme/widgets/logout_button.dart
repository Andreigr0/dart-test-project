import 'package:chat_mobile/services/user_service.dart';
import 'package:flutter/material.dart';

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.exit_to_app),
      tooltip: 'Logout',
      onPressed: UserService.instance.logout,
    );
  }
}
