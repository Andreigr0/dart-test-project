import 'package:chat_mobile/theme/widgets/logout_button.dart';
import 'package:chat_mobile/ui/chat_list/chat_list_view.dart';
import 'package:chat_mobile/ui/profile/profile_view.dart';
import 'package:chat_mobile/ui/users/users_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'chat_view_model.dart';

class ChatView extends StatelessWidget {
  const ChatView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ChatViewModel>(
      init: ChatViewModel(),
      builder: (model) {
        return DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('Chat View'),
              actions: <Widget>[
                IconButton(
                  key: const Key('person'),
                  icon: const Icon(Icons.person),
                  onPressed: () => Get.to(const ProfileView()),
                ),
                LogoutButton(),
              ],
              bottom: const TabBar(
                tabs: [
                  Tab(text: 'Chats'),
                  Tab(text: 'Users'),
                ],
              ),
            ),
            body: TabBarView(
              children: [
                ChatListView(selectedUsers: model.selectedUsers),
                UsersView(selectedUsers: model.selectedUsers),
              ],
            ),
          ),
        );
      },
    );
  }
}
