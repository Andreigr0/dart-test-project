import 'dart:async';

import 'package:chat_mobile/services/chat_service.dart';
import 'package:chat_mobile/ui/chat_content/chat_content_view.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_models/chat_models.dart';
import 'package:get/get.dart';

class ChatViewModel extends GetxBaseViewModel {
  StreamSubscription<Set<ChatId>> unreadSubscription;
  RxList<User> selectedUsers = <User>[].obs;

  @override
  void onInit() {
    super.onInit();

    unreadSubscription =
        ChatComponent.instance.unreadSubscription((unreadChatIds) {
      if (Get.currentRoute == '/ChatView' && unreadChatIds.isNotEmpty) {
        Get.snackbar(
          'You have a new message',
          'Tap to open the chat',
          onTap: (_) => Get.to(ChatContentView(chatId: unreadChatIds.first)),
        );
      }
    });
  }

  @override
  void onClose() {
    unreadSubscription?.cancel();
    super.onClose();
  }
}
