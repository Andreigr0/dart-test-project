import 'package:chat_mobile/theme/widgets/loading_state.dart';
import 'package:chat_mobile/ui/chat_content/chat_item.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

import '../../theme/widgets/logout_button.dart';
import 'chat_content_view_model.dart';

class ChatContentView extends StatelessWidget {
  final ChatId chatId;

  const ChatContentView({Key key, @required this.chatId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ChatContentViewModel>(
      init: ChatContentViewModel(chatId),
      builder: (model) {
        return Scaffold(
          appBar: AppBar(
            title:
                model.hasError || model.isBusy ? null : Text('${model.title}'),
            actions: [
              if (model.unreadChats.isNotEmpty)
                IconButton(
                  icon: const Icon(Icons.message),
                  tooltip: 'New messages',
                  color: Colors.greenAccent,
                  onPressed: Get.back,
                ),
              LogoutButton(),
            ],
          ),
          body: SafeArea(
            child: LoadingState<ChatContentViewModel>(
              child: Container(
                padding: const EdgeInsets.only(
                  left: 8.0,
                  right: 8.0,
                  bottom: 8.0,
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                        itemCount: model.messages.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ChatItem(message: model.messages[index]);
                        },
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            controller: model.textController,
                            decoration: const InputDecoration(
                              hintText: 'Your message',
                            ),
                            onSubmitted: model.textController.text.isNullOrBlank
                                ? null
                                : (_) => model.send(),
                          ),
                        ),
                        IconButton(
                          icon: const Icon(Icons.send),
                          onPressed: model.textController.text.isNullOrBlank
                              ? null
                              : model.send,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<ChatId>('chat', chatId));
  }
}
