import 'dart:async';
import 'dart:collection';

import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/services/chat_service.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/widgets.dart';

import '../../api_client.dart';

class ChatContentViewModel extends GetxBaseViewModel {
  String title;
  List<Message> messages = [];
  final textController = TextEditingController();

  StreamSubscription<Message> messagesSubscription;
  StreamSubscription<Set<ChatId>> unreadMessagesSubscription;
  final Set<ChatId> unreadChats = HashSet<ChatId>();

  Chat chat;
  final ChatId chatId;

  ChatContentViewModel(this.chatId);

  ChatComponent get chatService => ChatComponent.instance;

  @override
  Future<void> onInit() async {
    super.onInit();
    textController.addListener(update);
    await loadChat();
    title = chat.members
        .where((user) => user.id != currentUser.id)
        .map((user) => user.name)
        .join(", ");
    refreshChatContent();

    messagesSubscription = chatService.subscribeMessages(
      (receivedMessage) {
        messages.add(receivedMessage);
        update();
      },
      chatId,
    );

    unreadMessagesSubscription =
        chatService.unreadSubscription((unreadChatIds) {
      unreadChats.clear();
      unreadChats.addAll(unreadChatIds);
      update();
    });
  }

  @override
  void onClose() {
    textController.removeListener(update);
    messagesSubscription?.cancel();
    unreadMessagesSubscription?.cancel();
    super.onClose();
  }

  Future<void> refreshChatContent() async {
    return runFuture(() async {
      final msgList = await MessagesClient(MobileApiClient()).read(chatId);
      messages = msgList;
    });
  }

  Future<void> send() async {
    final newMessage = Message(
      chat: chatId,
      author: currentUser,
      text: textController.text,
      createdAt: DateTime.now(),
    );

    runFuture(() async {
      await MessagesClient(MobileApiClient()).create(newMessage);
      textController.clear();
    });
  }

  Future<void> loadChat() async {
    return runFuture(() async {
      // Since I've not found method for receiving only one `Chat` by id
      // I have to filter all of them
      final resp = await ChatsClient(MobileApiClient()).read({}) as List<Chat>;
      final found = resp.firstWhere((element) => element.id == chatId);
      if (found != null) {
        chat = found;
      }
    });
  }
}
