import 'package:chat_mobile/theme/widgets/loading_state.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'chat_list_view_model.dart';

class ChatListView extends StatefulWidget {
  final RxList<User> selectedUsers;

  const ChatListView({Key key, @required this.selectedUsers}) : super(key: key);

  @override
  _ChatListViewState createState() => _ChatListViewState();
}

class _ChatListViewState extends State<ChatListView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return GetBuilder<ChatListViewModel>(
      init: ChatListViewModel(widget.selectedUsers),
      builder: (model) {
        return LoadingState<ChatListViewModel>(
          child: RefreshIndicator(
            onRefresh: model.refreshChats,
            child: ListView.separated(
              itemBuilder: (context, index) {
                final chatItem = model.chats.elementAt(index);
                bool contains = false;
                for (final user in chatItem.members) {
                  contains = model.selectedUsersIds.contains(user.id);
                  if (contains) break;
                }

                return ListTile(
                  selected: contains,
                  selectedTileColor: Colors.grey[200],
                  leading: model.unreadChats.contains(chatItem.id)
                      ? const Icon(Icons.message)
                      : null,
                  title: Text(
                      chatItem.members.map((user) => user.name).join(" | ")),
                  onTap: () => model.onChatItemTap(chatItem),
                );
              },
              separatorBuilder: (context, index) => const Divider(height: 1),
              itemCount: model.chats.length,
            ),
          ),
        );
      },
    );
  }
}
