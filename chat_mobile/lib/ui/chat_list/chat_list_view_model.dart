import 'dart:async';
import 'dart:collection';

import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/ui/chat_content/chat_content_view.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_models/chat_models.dart';
import 'package:get/get.dart';

import '../../api_client.dart';
import '../../services/chat_service.dart';

class ChatListViewModel extends GetxBaseViewModel {
  final RxList<User> selectedUsers;

  List<UserId> get selectedUsersIds => selectedUsers.map((e) => e.id).toList();

  ChatListViewModel(this.selectedUsers);

  ChatComponent get chatService => ChatComponent.instance;

  List<Chat> chats = [];
  final Set<ChatId> unreadChats = HashSet<ChatId>();
  StreamSubscription<Set<ChatId>> unreadSubscription;
  StreamSubscription<List<User>> usersListener;

  Future<void> refreshChats() async {
    return runFuture(() async {
      // Just for demonstration purposes
      await Future.delayed(500.milliseconds);
      final found = await ChatsClient(MobileApiClient()).read({}) as List<Chat>;
      chats = found;
    });
  }

  @override
  Future<void> onInit() async {
    super.onInit();
    await refreshChats();
    unreadSubscription = chatService.unreadSubscription((unreadChatIds) {
      unreadChats.clear();
      unreadChats.addAll(unreadChatIds);
      update();
    });
    usersListener = selectedUsers.listen((_) => update());
  }

  @override
  void onClose() {
    unreadSubscription?.cancel();
    usersListener?.cancel();

    super.onClose();
  }

  void onChatItemTap(Chat chatItem) {
    Get.to(ChatContentView(chatId: chatItem.id));
  }
}
