import 'package:chat_mobile/theme/widgets/loading_state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login_view_model.dart';

class LoginView extends StatelessWidget {
  const LoginView();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginViewModel>(
      init: LoginViewModel(),
      builder: (model) {
        return Scaffold(
          body: Form(
            key: model.formKey,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    key: const Key('login'),
                    controller: model.loginController,
                    onFieldSubmitted: (value) => FocusScope.of(context)
                        .requestFocus(model.passwordFocus),
                    validator: model.validateLogin,
                    decoration: const InputDecoration(
                      hintText: 'Login',
                      labelText: 'Enter your login',
                    ),
                  ),
                  TextFormField(
                    key: const Key('password'),
                    controller: model.passwordController,
                    obscureText: true,
                    onFieldSubmitted: (value) => model.login(),
                    focusNode: model.passwordFocus,
                    validator: model.validatePassword,
                    decoration: const InputDecoration(
                      hintText: 'Password',
                      labelText: 'Enter your password',
                    ),
                  ),
                  const SizedBox(height: 20),
                  LoadingState<LoginViewModel>(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                          key: const Key('login_button'),
                          onPressed: model.login,
                          child: const Text("Login"),
                        ),
                        FlatButton(
                          key: const Key('sign_up_button'),
                          onPressed: model.signUp,
                          child: const Text("Sign up"),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
