import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/ui/chat/chat_view.dart';
import 'package:chat_mobile/utils/credentials_mixin.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../api_client.dart';

class LoginViewModel extends GetxBaseViewModel with CredentialsMixin {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Future<void> signUp() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();

      final bool resultValue = await Get.dialog(
        AlertDialog(
          content:
              Text("Do you want to create user '${loginController.text}' ?"),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Get.back(result: false),
              child: const Text("Cancel"),
            ),
            FlatButton(
              onPressed: () => Get.back(result: true),
              child: const Text("Ok"),
            ),
          ],
        ),
      );

      if (resultValue != true) {
        return;
      }

      await runFuture(() async {
        final UsersClient usersClient = UsersClient(MobileApiClient());

        final createdUser = await usersClient.create(User(
          name: loginController.text,
          password: passwordController.text,
        ));

        Get.snackbar('', "User '${createdUser.name}' created");
      });
      await login();
    }
  }

  Future<void> login() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();

      await runFuture(() async {
        final UsersClient usersClient = UsersClient(MobileApiClient());

        final user = await usersClient.login(
          loginController.text,
          passwordController.text,
        );

        UserService.instance.setUser(User(
          id: user.id,
          name: loginController.text,
          password: passwordController.text,
          phone: user.phone,
          email: user.email,
        ));
        Get.offAll(const ChatView());

        clearUi();
      }, onError: (error) async {
        Get.snackbar('', 'Login failed: ${error.message}');

        logger.e('Login failed');
        logger.e(error);
      });
    }
  }

  void clearUi() {
    loginController.clear();
    passwordController.clear();
  }
}
