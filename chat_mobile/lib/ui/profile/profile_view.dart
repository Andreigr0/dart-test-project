import 'package:chat_mobile/theme/widgets/loading_state.dart';
import 'package:chat_mobile/utils/hide_keyboard_on_scroll.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'profile_view_model.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileViewModel>(
      init: ProfileViewModel(),
      builder: (model) {
        return Scaffold(
          appBar: AppBar(title: const Text('Profile')),
          body: Form(
            key: model.formKey,
            child: NotificationListener(
              onNotification: hideKeyboard,
              child: ListView(
                padding: const EdgeInsets.all(16.0),
                children: <Widget>[
                  TextFormField(
                    key: const Key('login'),
                    controller: model.loginController,
                    decoration: const InputDecoration(
                      labelText: 'Login',
                    ),
                    onFieldSubmitted: (_) => FocusScope.of(context)
                        .requestFocus(model.passwordFocus),
                    validator: model.validateLogin,
                    readOnly: true,
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    key: const Key('password'),
                    controller: model.passwordController,
                    focusNode: model.passwordFocus,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      suffixIcon: IconButton(
                        icon: Icon(model.hidePassword
                            ? CupertinoIcons.eye
                            : CupertinoIcons.eye_fill),
                        onPressed: model.togglePasswordVisibility,
                      ),
                    ),
                    obscureText: model.hidePassword,
                    onFieldSubmitted: (_) =>
                        FocusScope.of(context).requestFocus(model.emailFocus),
                    validator: model.validatePassword,
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    key: const Key('email'),
                    controller: model.emailController,
                    focusNode: model.emailFocus,
                    decoration: const InputDecoration(
                      hintText: 'email@mail.com',
                      labelText: 'Email',
                    ),
                    onFieldSubmitted: (_) =>
                        FocusScope.of(context).requestFocus(model.phoneFocus),
                    keyboardType: TextInputType.emailAddress,
                    validator: model.validateEmail,
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    key: const Key('phone'),
                    controller: model.phoneController,
                    focusNode: model.phoneFocus,
                    decoration: const InputDecoration(
                      hintText: '+7 (900) 000-11-22',
                      labelText: 'Phone',
                    ),
                    keyboardType: TextInputType.phone,
                    onFieldSubmitted: (_) => model.send(),
                    validator: model.validatePhone,
                  ),
                  const SizedBox(height: 10),
                  LoadingState<ProfileViewModel>(
                    child: Center(
                      child: RaisedButton(
                        key: const Key('send'),
                        onPressed: model.send,
                        child: const Text('Send'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
