import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/utils/credentials_mixin.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:get/get.dart';

import '../../api_client.dart';

final RegExp phoneValidator =
    RegExp(r'^[+][7]\s[(][0-9]{3}[)]\s[0-9]{3}-[0-9]{2}-[0-9]{2}$');

final RegExp emailValidator = RegExp(
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+$");

class ProfileViewModel extends GetxBaseViewModel with CredentialsMixin {
  final formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final emailFocus = FocusNode();

  final TextEditingController phoneController = MaskedTextController(
    mask: '+7 (000) 000-00-00',
    text: '+7(',
  );

  final phoneFocus = FocusNode();

  bool hidePassword = true;

  UserService get userService => UserService.instance;

  User get user => userService.currentUser;

  @override
  void onInit() {
    super.onInit();
    loginController.text = user.name;
    emailController.text = user.email;
    phoneController.text = user.phone;
  }

  String validatePhone(String p) {
    String phone = p;
    if (phone.length > 18) {
      phone = phone.substring(0, 18);
    }

    if (!phoneValidator.hasMatch(phone)) {
      return 'Phone number should be in format +7 (900) 000-11-22';
    }
    return null;
  }

  void togglePasswordVisibility() {
    hidePassword = !hidePassword;
    update();
  }

  String validateEmail(String email) {
    if (!emailValidator.hasMatch(email)) {
      return 'Email is incorrect';
    }
    return null;
  }

  Future<void> send() async {
    if (formKey.currentState.validate()) {
      final updated = User(
        id: user.id,
        name: loginController.text,
        password: passwordController.text,
        email: emailController.text,
        phone: phoneController.text,
      );
      await runFuture(() async {
        final UsersClient usersClient = UsersClient(MobileApiClient());

        await usersClient.update(updated);
        userService.setUser(updated);
        Get.back();
        Get.dialog(const AlertDialog(
          content: Text('Information updated'),
        ));
      });
    }
  }
}
