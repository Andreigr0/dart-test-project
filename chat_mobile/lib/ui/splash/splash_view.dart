import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'splash_view_model.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashViewModel>(
      init: SplashViewModel(),
      builder: (model) => const Scaffold(),
    );
  }
}
