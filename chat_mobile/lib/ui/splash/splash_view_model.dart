import 'package:chat_mobile/repositories/user_repository.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class SplashViewModel extends GetxBaseViewModel {
  @override
  Future<void> onInit() async {
    super.onInit();
    await Get.putAsync<UserRepository>(() async {
      await Hive.openBox(UserRepository.userBoxKey);
      return UserRepository();
    });
    Get.put(UserService());
  }
}
