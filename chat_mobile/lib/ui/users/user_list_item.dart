import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';

class UserListItem extends StatelessWidget {
  final User user;
  final bool selected;
  final VoidCallback onTap;

  const UserListItem({
    Key key,
    @required this.user,
    @required this.selected,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final name = user.name ?? "No Name";
    String letters =
        name.split(' ').map((e) => e.substring(0, 1)).join().toUpperCase();

    if (letters.length > 2) {
      letters = letters.substring(0, 2);
    }

    return ListTile(
      leading: CircleAvatar(child: Text(letters)),
      title: Text(name),
      subtitle: user.email != null ? Text(user.email) : null,
      selected: selected,
      selectedTileColor: Colors.grey[200],
      trailing: Checkbox(
        value: selected,
        onChanged: (_) => onTap(),
      ),
      onTap: onTap,
    );
  }
}
