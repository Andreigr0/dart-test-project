import 'package:chat_mobile/theme/widgets/loading_state.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'user_list_item.dart';
import 'users_view_model.dart';

class UsersView extends StatefulWidget {
  final RxList<User> selectedUsers;

  const UsersView({Key key, @required this.selectedUsers}) : super(key: key);

  @override
  _UsersViewState createState() => _UsersViewState();
}

class _UsersViewState extends State<UsersView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return GetBuilder<UsersViewModel>(
      init: UsersViewModel(widget.selectedUsers),
      builder: (model) {
        return Stack(
          children: <Widget>[
            Positioned.fill(
              child: LoadingState<UsersViewModel>(
                child: RefreshIndicator(
                  onRefresh: model.refreshUsers,
                  child: ListView.separated(
                    separatorBuilder: (context, index) =>
                        const Divider(height: 1),
                    itemCount: model.foundUsers.length,
                    itemBuilder: (BuildContext context, int index) {
                      final user = model.foundUsers.elementAt(index);
                      return UserListItem(
                        user: user,
                        selected: model.selectedUsers.contains(user),
                        onTap: () => model.selectUser(user),
                      );
                    },
                  ),
                ),
              ),
            ),
            if (model.selectedUsers.isNotEmpty)
              SafeArea(
                child: Container(
                  padding: const EdgeInsets.only(bottom: 16, right: 16),
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton(
                    heroTag: 'create_chat',
                    onPressed: model.createChat,
                    child: const Icon(Icons.add),
                  ),
                ),
              ),
          ],
        );
      },
    );
  }
}
