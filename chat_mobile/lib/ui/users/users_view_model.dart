import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/ui/chat_content/chat_content_view.dart';
import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:get/get.dart';

import '../../api_client.dart';

class UsersViewModel extends GetxBaseViewModel {
  final RxList<User> selectedUsers;

  UsersViewModel(this.selectedUsers);

  List<User> foundUsers = [];

  @override
  void onInit() {
    super.onInit();
    refreshUsers();
  }

  Future<void> refreshUsers() async {
    return runFuture(() async {
      selectedUsers.clear();
      // Just for demonstration purposes
      await Future.delayed(500.milliseconds);

      final UsersClient usersClient = UsersClient(MobileApiClient());
      final List<User> found = await usersClient.read({}) as List<User>;

      found.removeWhere((user) => user.id == currentUser.id);

      foundUsers = found;
      update();
    });
  }

  Future<void> createChat() async {
    if (selectedUsers.isNotEmpty) {
      try {
        final ChatsClient chatsClient = ChatsClient(MobileApiClient());
        final Chat createdChat = await chatsClient.create(
          Chat(members: selectedUsers..add(currentUser)),
        );

        selectedUsers.clear();
        update();
        Get.to(ChatContentView(chatId: createdChat.id));
      } on Exception catch (e) {
        logger.e('Chat creation failed');
        logger.e(e);
      }
    }
  }

  void selectUser(User user) {
    if (selectedUsers.contains(user)) {
      selectedUsers.remove(user);
    } else {
      selectedUsers.add(user);
    }
    update();
  }
}
