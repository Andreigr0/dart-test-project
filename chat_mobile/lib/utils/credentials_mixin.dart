import 'package:chat_mobile/utils/getx_base_view_model.dart';
import 'package:flutter/material.dart';

mixin CredentialsMixin on GetxBaseViewModel {
  final TextEditingController loginController = TextEditingController();
  final passwordFocus = FocusNode();
  final TextEditingController passwordController = TextEditingController();

  String validateLogin(String value) {
    if (value.length < 2) {
      // check login rules here
      return 'The Login must be at least 2 characters.';
    }
    return null;
  }

  String validatePassword(String value) {
    if (value.length < 2) {
      // check password rules here
      return 'The Password must be at least 2 characters.';
    }
    return null;
  }
}
