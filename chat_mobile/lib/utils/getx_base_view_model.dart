import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'logger.dart';

class GetxBaseViewModel extends GetxController {
  String errorMessage;

  bool get hasError => errorMessage != null;
  bool _isBusy = false;

  bool get isBusy => _isBusy;

  bool get showDialogOnError => true;

  Future<void> runFuture(
    Future<void> Function() body, {
    Future<void> Function(dynamic error) onError,
  }) async {
    if (!_isBusy) {
      try {
        setBusy(true);
        errorMessage = null;
        await body();
      } on DioError catch (e) {
        if (e.type == DioErrorType.RESPONSE) {
          final errors = e.response.data['errors'] as List;
          // ignore: unnecessary_string_interpolations
          errorMessage = '${errors?.join('\n')}';

          if (showDialogOnError) {
            Get.dialog(
              CupertinoAlertDialog(
                content: Column(
                  children: [for (final error in errors) Text('$error')],
                ),
              ),
            );
          }
        }
      } catch (e) {
        logger.e(e);
        errorMessage = 'Unknown error';

        // Workaround. I would use `Dio` and add to it Interceptor
        // so it could retry request
        if (e is HttpException) {
          if (e.statusCode == 401 && Get.currentRoute != '/LoginView') {
            try {
              await UserService.instance.refreshToken();
              return;
            } catch (e) {
              errorMessage = "Can't update token";
              UserService.instance.logout();
            }
          }
        }

        if (showDialogOnError) {
          Get.dialog(CupertinoAlertDialog(
            content: Text(errorMessage),
          ));
        }
        if (onError != null) {
          await onError(e);
        }
        rethrow;
      } finally {
        setBusy(false);
      }
    }
  }

  // ignore: avoid_positional_boolean_parameters
  void setBusy(bool value) {
    _isBusy = value;
    if (_isBusy) {
      errorMessage = null;
    }
    update();
  }
}
