import 'package:chat_mobile/repositories/user_repository.dart';
import 'package:chat_mobile/services/chat_service.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/ui/chat/chat_view.dart';
import 'package:chat_mobile/ui/chat_list/chat_list_view_model.dart';
import 'package:chat_models/src/user.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:mockito/mockito.dart';

class _MockChatComponent extends Mock implements ChatComponent {}

class _MockUserRepository extends Mock implements UserRepository {}

class _MockUserService extends Mock implements UserService {
  @override
  User currentUser = User(
    name: 'Имя пользователя',
    email: 'email@email.com',
    phone: '+7 (900) 000-11-22',
  );
}

class _MockChatList extends ChatListViewModel {
  _MockChatList() : super(<User>[].obs);

  @override
  Future<void> refreshChats() async {
    // super.refreshChats();
    return runFuture(() async {
      await Future.delayed(300.milliseconds);
      chats = [];
    });
  }
}

void main() {
  UserService userService;

  setUp(() async {
    Get.put<ChatComponent>(_MockChatComponent());
    Get.put<UserRepository>(_MockUserRepository());
    userService = Get.put<UserService>(_MockUserService());
    Get.put<ChatListViewModel>(_MockChatList());
  });

  testWidgets('tap on [profile] button', (tester) async {
    await tester.pumpWidget(const GetMaterialApp(home: ChatView()));

    await tester.runAsync(() async {
      expect(Get.currentRoute, '/');

      await tester.tap(find.byKey(const Key('person')));
      await tester.pumpAndSettle(300.milliseconds);
      expect(Get.currentRoute, '/ProfileView');
    });
  });

  testWidgets('tap on [logout] button', (tester) async {
    await tester.pumpWidget(const GetMaterialApp(home: ChatView()));
    when(userService.logout()).thenAnswer((_) async {
      userService.currentUser = null;
    });

    await tester.runAsync(() async {
      expect(userService.currentUser, isNotNull);

      expect(find.byTooltip('Logout'), findsOneWidget);
      await tester.tap(find.byTooltip('Logout'));
      verify(userService.logout());

      expect(userService.currentUser, isNull);
    });
  });
}
