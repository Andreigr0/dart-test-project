import 'package:chat_mobile/repositories/user_repository.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/ui/login/login_view.dart';
import 'package:chat_mobile/ui/login/login_view_model.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/src/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class _UserRepositoryMock implements UserRepository {
  @override
  Future<void> clear() async {
    logger.d('Cleared');
  }

  @override
  Future<String> getToken() async {
    // return 'token';
    return null;
  }

  @override
  Future<User> getUser() async {
    // return User();
    return null;
  }

  @override
  Future<void> saveToken(String userToken) async {
    logger.d('Saved token $userToken');
  }

  @override
  Future<void> saveUser(User user) async {
    logger.d('Saved user ${user?.json}');
  }

  @override
  Box get userBox => throw UnimplementedError();
}

class _LoginViewModelMock extends LoginViewModel {
  @override
  Future<void> login() async {
    return runFuture(() async {
      return UserService.instance.setUser(User(
        name: loginController.text,
        password: passwordController.text,
      ));
    });
  }
}

class _UserServiceMock extends UserService {
  UserRepository get _userRepo => Get.find<UserRepository>();

  @override
  Future<void> onInit() async {
    authToken = await _userRepo.getToken();
    this.currentUser = await _userRepo.getUser();
  }
}

void main() {
  setUp(() async {
    Get.testMode = true;

    await Get.putAsync<UserRepository>(() async => _UserRepositoryMock());
    Get.put<UserService>(_UserServiceMock());

    Get.lazyPut<LoginViewModel>(() => _LoginViewModelMock());
  });

  testWidgets('Test 2 TextFields and 2 buttons', (tester) async {
    await tester.pumpWidget(const GetMaterialApp(home: LoginView()));

    expect(find.byType(TextFormField), findsNWidgets(2));
    expect(find.byType(RaisedButton), findsOneWidget);
    expect(find.byType(FlatButton), findsOneWidget);
  });

  testWidgets('Test Login?', (WidgetTester tester) async {
    await tester.pumpWidget(
      const GetMaterialApp(
        home: LoginView(),
      ),
    );

    await tester.runAsync(() async {
      final viewModel = Get.find<LoginViewModel>();

      expect(viewModel.loginController.text.isEmpty, true);
      expect(viewModel.passwordController.text.isEmpty, true);

      await tester.enterText(find.byKey(const Key('login')), 'text');
      await tester.enterText(find.byKey(const Key('password')), 'password');

      expect(viewModel.loginController.text, 'text');
      expect(viewModel.passwordController.text, 'password');

      await tester.tap(find.byKey(const Key('login_button')));

      final user = UserService.instance.currentUser;

      expect(user.name, equals(viewModel.loginController.text));
      expect(user.password, equals(viewModel.passwordController.text));
    });
  });
}
