import 'package:chat_mobile/repositories/user_repository.dart';
import 'package:chat_mobile/services/user_service.dart';
import 'package:chat_mobile/ui/profile/profile_view.dart';
import 'package:chat_mobile/ui/profile/profile_view_model.dart';
import 'package:chat_mobile/utils/logger.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:mockito/mockito.dart';

final _user = User(
  name: 'Имя пользователя',
  email: 'email@email.com',
  phone: '+7 (900) 000-11-22',
);

class _RepoMock extends Mock implements UserRepository {}

class _MockUserService extends UserService {
  @override
  // ignore: overridden_fields
  User currentUser = _user;

  @override
  Future<void> onInit() async {}
}

class _MockProfileViewModel extends ProfileViewModel {
  @override
  Future<void> send() async {
    if (formKey.currentState.validate()) {
      final updated = User(
        id: user.id,
        name: loginController.text,
        password: passwordController.text,
        email: emailController.text,
        phone: phoneController.text,
      );
      await runFuture(() async {
        await Future.delayed(300.milliseconds);
        userService.setUser(updated);
        // Get.back();
        Get.dialog(const AlertDialog(
          content: Text('Information updated'),
        ));
      });
    }
  }
}

void main() {
  setUp(() async {
    Get.put<UserService>(_MockUserService());
    expect(Get.isRegistered<ProfileViewModel>(), false);
  });

  testWidgets(
    "Form filled with the user's data except password",
    (tester) async {
      await tester.pumpWidget(const GetMaterialApp(home: ProfileView()));
      expect(Get.isRegistered<ProfileViewModel>(), true);

      final vm = Get.find<ProfileViewModel>();

      expect(vm.loginController.text, _user.name);
      expect(vm.emailController.text, _user.email);
      expect(vm.phoneController.text, _user.phone);
      expect(vm.passwordController.text.isEmpty, true);
      expect(vm.hidePassword, true);
    },
  );

  testWidgets('Save new data', (tester) async {
    Get.lazyPut<ProfileViewModel>(() => _MockProfileViewModel());
    Get.lazyPut<UserRepository>(() => _RepoMock());

    await tester.pumpWidget(const GetMaterialApp(home: ProfileView()));
    expect(Get.isRegistered<ProfileViewModel>(), true);

    final vm = Get.find<ProfileViewModel>();

    const email = 'some@email.com';
    await tester.enterText(find.byKey(const Key('email')), email);
    expect(vm.emailController.text, equals(email));
    expect(vm.user.email, isNot(equals(email)));

    const phone = '+7 (901) 000-11-33';
    await tester.enterText(find.byKey(const Key('phone')), phone);
    expect(vm.phoneController.text, equals(phone));
    expect(vm.user.phone, isNot(equals(phone)));

    const password = 'password';
    await tester.enterText(find.byKey(const Key('password')), password);
    expect(vm.passwordController.text, equals(password));

    await tester.runAsync(() async {
      expect(vm.formKey.currentState.validate(), true);
      await tester.tap(find.byKey(const Key('send')));
      await tester.pump();

      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      await Future.delayed(300.milliseconds);
      await tester.pump();

      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(Get.isDialogOpen, true);
    });

    final currentUser = vm.userService.currentUser;
    expect(currentUser.name, equals(vm.loginController.text));
    expect(currentUser.password, equals(vm.passwordController.text));
    expect(currentUser.phone, equals(vm.phoneController.text));
    expect(currentUser.email, equals(vm.emailController.text));
  });
}
