import 'package:chat_mobile/repositories/user_repository.dart';
import 'package:chat_mobile/ui/splash/splash_view.dart';
import 'package:chat_mobile/ui/splash/splash_view_model.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:mockito/mockito.dart';

class _RepoMock extends Mock implements UserRepository {}

void main() {
  UserRepository repo;

  setUp(() async {
    Hive.init('test');
    repo = await Get.putAsync<UserRepository>(() async => _RepoMock());

    expect(Get.isRegistered<UserRepository>(), true);
    expect(Get.isRegistered<SplashViewModel>(), false);
  });

  testWidgets(
    'Go to [LoginView] when there is NO saved user data',
    (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(const GetMaterialApp(
          home: SplashView(),
        ));
        expect(Get.isRegistered<SplashViewModel>(), true);

        await Get.find<SplashViewModel>().onInit();

        final repo = Get.find<UserRepository>();

        expect(await repo.getUser(), null);
        expect(await repo.getToken(), null);

        expect(Get.currentRoute, '/LoginView');
      });
    },
  );

  testWidgets(
    'Go to [ChatView] when there is saved user data',
    (WidgetTester tester) async {
      when(repo.getToken()).thenAnswer((_) async => 'token');
      when(repo.getUser()).thenAnswer((_) async => User(name: 'User'));

      await tester.runAsync(() async {
        await tester.pumpWidget(const GetMaterialApp(
          home: SplashView(),
        ));
        expect(Get.isRegistered<SplashViewModel>(), true);

        await Get.find<SplashViewModel>().onInit();

        expect(await repo.getUser(), User(name: 'User'));
        expect(await repo.getToken(), 'token');

        expect(Get.currentRoute, '/ChatView');
      });
    },
  );
}
