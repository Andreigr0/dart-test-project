import 'package:data_model/data_model.dart';
import 'package:equatable/equatable.dart';

class User extends Equatable implements Model<UserId> {
  UserId id;
  String name;
  String password;
  String phone;
  String email;

  User({
    this.id,
    this.name,
    this.password,
    this.phone,
    this.email,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }

    return User(
      id: UserId(json['id']),
      name: json['name'],
      password: json['password'],
      phone: json['phone'],
      email: json['email'],
    );
  }

  Map<String, dynamic> get json {
    return {
      'id': id?.json,
      'name': name,
      'password': password,
      'phone': phone,
      'email': email,
    }..removeWhere((key, value) => value == null);
  }

  @override
  List<Object> get props => [id, name, password, phone, email];
}

class UserId extends ObjectId {
  UserId._(id) : super(id);
  factory UserId(id) {
    if (id == null) return null;
    return UserId._(id);
  }
}
